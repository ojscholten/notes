# Unlimited Memory
by Kevin Horsley

This book presents a collection of memory techniques which can be used to recall
sequences, collections, and other types of information. The techniques are
interesting because they can be used together, and become more and more powerful
the more different techniques are used.

Before covering the techniques, they are broadly applied using the **SEE principle**; 
use your (S)enses to really 'experience' the thing you want to
remember, (E)xaggerate the experience you are feeling so that it's beyond
something you would encounter in daily life, and (E)nergize the information -
make it vivid, funny, colourful, and illogical. The more extreme the scenario,
the easier it will be to remember.

The techniques are (simplified);
1. The Car Method (place objects around a known vehicle (car) and recall the
placement process)
2. The Body Method (same as car but with your own body)
3. Rhyming Peg Method (create numerical memory 'pegs' using rhyming words,
and incorporate those words into the content, e.g. three = tree)
4. Shape System (create numerical memory 'pegs' using shapes identical to the
numbers, e.g. snowman = number 8)
5. The Journey Method (mentally place objects along a well known route)
6. Linking Thoughts (create a mental film of interacting objects)
7. Encoding (for numbers, you can encode pairs, triplets, etc using the shape
or rhyming systems above, and can build custom vocabularies which map to
numbers)

They can be used together as follows; say I want to remember 30 Python keywords,
I can break them down into groups of 5 and use the body method to 'place' one on
each leg, one on each arm, and one on my head. I can then place multiple
variants of my body (maybe an alien version, a smurf version, etc) at 6
different points around a car (in front, driver seat, passenger seat, back left,
back right, and boot). Using these 5 bodies at 6 locations I can 'place' all the
keywords and a short mental film presenting each one on each body part at each
location, for a total of 30 unique memory locations.

This approach scales combinatorially, for example, I can have a version of the
shape system at each of the five body locations, expanding the storage capacity
tenfold. The final (Encoding) system does this in a more systematic way, but I
haven't studied that deeper as it seems better suited to remembering long
homogeneous sequences which isn't something I need right now. 

## Quick note on remembering names;

Concentrate (on the name), Create (a strong rhyming scene), Connect (that
scene to the person, person to other people you know, their face to the scene,
or the meeting location to person), and Continually use it and review it. This
isn't that useful if you aren't meeting lots of new people regularly, but it's a
neat tool to have.

