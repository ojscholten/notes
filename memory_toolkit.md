# Memory Toolkit

In order to increase your ability to retain and recall information, you need a set of tools;

## Memory Palace

A [memory palace](https://www.youtube.com/watch?v=Yw4x_Ijoz1M) is a mental model of a physical location. See [this post](https://mullenmemory.com/blog/2015/7/21/making-memory-palaces-keep-it-simple) by memory champion Alex Mullen on how to build/review them.

## Station

A memory station is a location within a palace which contains a single [piece of information](https://www.youtube.com/watch?v=tJ5TsOchf7A).

## Journey

A journey is a fixed path through a memory palace, which passes through [all stations](https://www.youtube.com/watch?v=nwoE999Uab0) in the palace.

These three tools can be used to create mental routes through 'museum' type constructs, allowing sequential storage of arbitrary information. See my reading notes on [Unlimited Memory](unlimited_memory.md) for how they can be combined with other techniques.