# General Notes

This repo contains general non-technical notes on side projects, reading, and useful links to tools and interesting pages.

# Reading Notes

[Unlimited Memory](unlimited_memory.md) 

# Non-technical Technical Tools

The [McCabe Python module](https://github.com/pycqa/mccabe) can be used to
assess the cyclomatic complexity of a Python module.

The [prospector](https://prospector.landscape.io/en/master/) module can be used
to automatically recommend improvements to code quality.

# Useful Links

[Diagrams.net](https://app.diagrams.net) is a great tool for creating diagrams.

[OpenAI's Playground](https://beta.openai.com/playground) can be used to interact with GPT3. 

[SankeyMATIC](https://www.sankeymatic.com/build/) is a useful tool for building Sankey diagrams.
